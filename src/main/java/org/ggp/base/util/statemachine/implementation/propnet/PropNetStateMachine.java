package org.ggp.base.util.statemachine.implementation.propnet;

import java.util.*;

import org.ggp.base.util.game.TestGameRepository;
import org.ggp.base.util.gdl.grammar.*;
import org.ggp.base.util.logging.GamerLogger;
import org.ggp.base.util.propnet.architecture.Component;
import org.ggp.base.util.propnet.architecture.PropNet;
import org.ggp.base.util.propnet.architecture.components.Constant;
import org.ggp.base.util.propnet.architecture.components.Proposition;
import org.ggp.base.util.propnet.factory.LegacyPropNetFactory;
import org.ggp.base.util.propnet.factory.OptimizingPropNetFactory;
import org.ggp.base.util.statemachine.MachineState;
import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.statemachine.StateMachine;
import org.ggp.base.util.statemachine.exceptions.GoalDefinitionException;
import org.ggp.base.util.statemachine.exceptions.MoveDefinitionException;
import org.ggp.base.util.statemachine.exceptions.TransitionDefinitionException;
import org.ggp.base.util.statemachine.implementation.prover.query.ProverQueryBuilder;


@SuppressWarnings("unused")
public class PropNetStateMachine extends StateMachine {
    /** The underlying proposition network  */
    private PropNet propNet;
    /** The topological ordering of the propositions */
    private List<Proposition> ordering;
    /** The player roles */
    private List<Role> roles;

    /**
     * Initializes the PropNetStateMachine. You should compute the topological
     * ordering here. Additionally you may compute the initial state here, at
     * your discretion.
     */
    @Override
    public void initialize(List<Gdl> description) {
        try {
			propNet = OptimizingPropNetFactory.create(description);
	        roles = propNet.getRoles();
	        ordering = getOrdering();

            GamerLogger.log("PropNetStateMachine", "built propnet");
            // propNet.renderToFile("propnet.dot");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
    }

    /**
     * Computes the value of all propositions of the propnet
     * basePropositions (and inputPropositions) should be set beforehand
     */
    private void computePropNet() {
        for (Proposition prop : ordering) {
            prop.setValue(prop.getSingleInput().getValue());
        }
    }

    /**
     * Set the value of base propositions base on state
     */
    private void setBasePropositions(MachineState state) {
        Map<GdlSentence, Proposition> baseProps = propNet.getBasePropositions();
        Set<GdlSentence> contents = state.getContents();

        for (GdlSentence cont : contents) {
            baseProps.get(cont).setValue(true);
        }
    }

	/**
	 * Computes if the state is terminal. Should return the value
	 * of the terminal proposition for the state.
     * - set the basePropositions according to state
     * - compute others
     * - check the value of the terminalProposition
	 */
	@Override
	public boolean isTerminal(MachineState state) {
        propNet.resetAllPropositions();

        setBasePropositions(state);

        computePropNet();

        return propNet.getTerminalProposition().getValue();
	}

	/**
	 * Computes the goal for a role in the current state.
	 * Should return the value of the goal proposition that
	 * is true for that role. If there is not exactly one goal
	 * proposition true for that role, then you should throw a
	 * GoalDefinitionException because the goal is ill-defined.
     * - set basePropositions according to state
     * - check the true goalPropositions for that role
     * - return the value (use getGoalValue)
	 */
	@Override
	public int getGoal(MachineState state, Role role)
	throws GoalDefinitionException {
        propNet.resetAllPropositions();
        setBasePropositions(state);
        computePropNet();

        Set<Proposition> goalPropositions = propNet.getGoalPropositions().get(role);
        for (Proposition prop : goalPropositions) {
            if (prop.getValue()) {
                return getGoalValue(prop);
            }
        }

		throw new GoalDefinitionException(state, role);
	}

	/**
	 * Returns the initial state. The initial state can be computed
	 * by only setting the truth value of the INIT proposition to true,
	 * and then computing the resulting state.
     * - propnet.getInitProposition(), set to true
     * - compute the rest
     * - getStateFromBase()
	 */
	@Override
	public MachineState getInitialState() {
        propNet.resetAllPropositions();
        Proposition initProp = propNet.getInitProposition();
        if (initProp != null) {
            initProp.setValue(true);
            computePropNet();
            computeBasePropositions();
        }

		return getStateFromBase();
	}

	/**
	 * Computes the legal moves for role in state.
     * - set base propositions according to state
     * - get the legalPropositions that are true for that role
     * - translate the inputPropositions into Move (use getMoveFromProposition)
	 */
	@Override
	public List<Move> getLegalMoves(MachineState state, Role role)
	throws MoveDefinitionException {
		propNet.resetAllPropositions();
        setBasePropositions(state);

        computePropNet();

        Set<Proposition> legalProps = propNet.getLegalPropositions().get(role);
        List<Move> moves = new ArrayList<>();

        for (Proposition prop : legalProps) {
            if (prop.getValue()) {
                moves.add(getMoveFromProposition(prop));
            }
        }

		return moves;
	}

    /**
     * Set value of inputPropositions
     * @param moves - list of moves, listed in roles' order
    */
    private void setInputPropositions(List<Move> moves) {
        Map<GdlSentence, Proposition> inputPropositions = propNet.getInputPropositions();
        List<GdlSentence> doeses = toDoes(moves);
        for (GdlSentence does : doeses) {
            inputPropositions.get(does).setValue(true);
        }
    }


    /**
     * Compute new values for basePropositions
     */
    private void computeBasePropositions() {
        for (Proposition prop : propNet.getBasePropositions().values()) {
            prop.setValue(prop.getSingleInput().getValue());
        }
    }

	/**
	 * Computes the next state given state and the list of moves.
     * - set the basePropositions
     * - set the inputPropositions
     * - compute new basePropositions
	 */
	@Override
	public MachineState getNextState(MachineState state, List<Move> moves)
	        throws TransitionDefinitionException {
		propNet.resetAllPropositions();

        // initialize the current state
        setBasePropositions(state);
        setInputPropositions(moves);

        computePropNet();

        // compute new base propositions
        computeBasePropositions();

        // extract new state
        return getStateFromBase();
	}

	/**
	 * This should compute the topological ordering of propositions.
	 * Each component is either a proposition, logical gate, or transition.
	 * Logical gates and transitions only have propositions as inputs.
	 *
	 * The base propositions and input propositions should always be exempt
	 * from this ordering.
	 *
	 * The base propositions values are set from the MachineState that
	 * operations are performed on and the input propositions are set from
	 * the Moves that operations are performed on as well (if any).
	 *
	 * @return The order in which the truth values of propositions need to be set.
     * - go through every components except base, input, and init.
     * - add all components that has all its inputs computed to readyComps Queue
     * - add all components and their inputs' size to compSet Set
     * - BFS
	 */
	public List<Proposition> getOrdering()
        {
            // List to contain the topological ordering.
            List<Proposition> order = new LinkedList<Proposition>();

            // All of the components in the PropNet
            List<Component> components = new ArrayList<Component>(propNet.getComponents());

            // All of the propositions in the PropNet.
            List<Proposition> propositions = new ArrayList<Proposition>(propNet.getPropositions());

            // Map of base propositions
            Map<GdlSentence, Proposition> baseProps = propNet.getBasePropositions();

            // Map of input propositions
            Map<GdlSentence, Proposition> inputProps = propNet.getInputPropositions();

            Map<Component, Integer> componentUncomputedInputMap = new HashMap<>();

            // add constant elements
            Set<Component> addedComponentSet = new HashSet<>();
            Queue<Component> compQueue = new ArrayDeque<>();
            for (Component comp : components) {
                if (comp instanceof Constant) {
                    compQueue.add(comp);
                    addedComponentSet.add(comp);
                }
            }
//            GamerLogger.log("PropNet", "No of Constant: " + compQueue.size());

            // add base and input propositions
            Proposition init = propNet.getInitProposition();
            compQueue.add(init);
            addedComponentSet.add(init);

            for (Component prop : baseProps.values()) {
                compQueue.add(prop);
                addedComponentSet.add(prop);
            }


            for (Component prop : inputProps.values()) {
                compQueue.add(prop);
                addedComponentSet.add(prop);
            }

            // add other components to queue
            for (Component comp : components) {
                if (addedComponentSet.contains(comp)) {
                    continue;
                }
                componentUncomputedInputMap.put(comp, comp.getInputs().size());
            }

//            GamerLogger.log("PropNet", "total components: " + components.size());
//            GamerLogger.log("PropNet", "and components: " + propNet.getNumAnds());
//            GamerLogger.log("PropNet", "or components: " + propNet.getNumOrs());
//            GamerLogger.log("PropNet", "not components: " + propNet.getNumNots());
//            GamerLogger.log("PropNet", "link components: " + propNet.getNumLinks());
//            GamerLogger.log("PropNet", "Size of uncomputed components set: " + componentUncomputedInputMap.size());
//            GamerLogger.log("PropNet", "Size of added set: " + addedComponentSet.size());


            Component comp;
            Set<Component> outputs;
            int t;
            int count_comps = 0;
            while (!compQueue.isEmpty()) {
                comp = compQueue.poll();

                outputs = comp.getOutputs();
                for (Component c : outputs) {
                    if (!componentUncomputedInputMap.containsKey(c)) {
                        continue;
                    }
                    t = componentUncomputedInputMap.get(c);
                    componentUncomputedInputMap.put(c,t-1);
                    if (t==1) { // t-1 = 0
                        if (!addedComponentSet.contains(c)) {
                            if (c instanceof Proposition) {
                                order.add((Proposition)c);
                            } else {
                                count_comps ++;
                            }
                            compQueue.add(c);
                            addedComponentSet.add(c);
                        }
                    }
                }
            }

//            GamerLogger.log("PropNet", "Size of added set: " + addedComponentSet.size());
//            GamerLogger.log("PropNet", "No in ordering: " + order.size());
//            GamerLogger.log("PropNet", "No of comps: " + count_comps);
            int count = 0;
            for (Component c : componentUncomputedInputMap.keySet()) {
                if (componentUncomputedInputMap.get(c) > 0) {
                    count ++;
                    if (c instanceof Proposition) {
                        GamerLogger.log("PropNet", ((Proposition)c).getName().toString());
                    }
                }
            }
//            GamerLogger.log("PropNet", "No of uncompute: " + count);

            return order;
	}

	/* Already implemented for you */
	@Override
	public List<Role> getRoles() {
		return roles;
	}

	/* Helper methods */

	/**
	 * The Input propositions are indexed by (does ?player ?action).
	 *
	 * This translates a list of Moves (backed by a sentence that is simply ?action)
	 * into GdlSentences that can be used to get Propositions from inputPropositions.
	 * and accordingly set their values etc.  This is a naive implementation when coupled with
	 * setting input values, feel free to change this for a more efficient implementation.
	 *
	 * @param moves
	 * @return
	 */
	private List<GdlSentence> toDoes(List<Move> moves)
	{
		List<GdlSentence> doeses = new ArrayList<GdlSentence>(moves.size());
		Map<Role, Integer> roleIndices = getRoleIndices();

		for (int i = 0; i < roles.size(); i++)
		{
			int index = roleIndices.get(roles.get(i));
			doeses.add(ProverQueryBuilder.toDoes(roles.get(i), moves.get(index)));
		}
		return doeses;
	}

	/**
	 * Takes in a Legal Proposition and returns the appropriate corresponding Move
	 * @param p
	 * @return a PropNetMove
	 */
	public static Move getMoveFromProposition(Proposition p)
	{
		return new Move(p.getName().get(1));
	}

	/**
	 * Helper method for parsing the value of a goal proposition
	 * @param goalProposition
	 * @return the integer value of the goal proposition
	 */
    private int getGoalValue(Proposition goalProposition)
	{
		GdlRelation relation = (GdlRelation) goalProposition.getName();
		GdlConstant constant = (GdlConstant) relation.get(1);
		return Integer.parseInt(constant.toString());
	}

	/**
	 * A Naive implementation that computes a PropNetMachineState
	 * from the true BasePropositions.  This is correct but slower than more advanced implementations
	 * You need not use this method!
	 * @return PropNetMachineState
	 */
	public MachineState getStateFromBase()
	{
		Set<GdlSentence> contents = new HashSet<GdlSentence>();
		for (Proposition p : propNet.getBasePropositions().values())
		{
   			// p.setValue(p.getSingleInput().getValue());
			if (p.getValue())
			{
				contents.add(p.getName());
			}

		}
		return new MachineState(contents);
	}

    public static void main(String[] args) throws Exception{
        PropNetStateMachine sm = new PropNetStateMachine();
        // List<Gdl> desc = new TestGameRepository().getGame("ticTacToe").getRules();
        // List<Gdl> desc = new TestGameRepository().getGame("test_case_3c").getRules();
        // List<Gdl> desc = new TestGameRepository().getGame("test_case_1a").getRules();
        //* List<Gdl> desc = new TestGameRepository().getGame("test_case_5b").getRules();
        List<Gdl> desc = new TestGameRepository().getGame("test_case_5a").getRules();
        for (Gdl gdl : desc) {
            System.out.println(gdl);
        }
        if (false) {
            return;
        }
        sm.initialize(desc);
        sm.propNet.renderToFile("test_case_5a.dot");
        // base
        System.out.println("BASES");
        for (Proposition prop : sm.propNet.getBasePropositions().values()) {
            System.out.println(prop.getName());
        }
        // print topological orders
        System.out.println("TOPOLOGICAL ORDER");
        for(Proposition prop : sm.ordering) {
            System.out.println(prop.getName());
        }
        MachineState state = sm.getInitialState();
        System.out.println(state);
        Role you = new Role(GdlPool.getConstant("you"));
        System.out.println("TERMINAL: " + sm.isTerminal(state));
        System.out.println("Should have 1 move (proceed): " + sm.getLegalMoves(state, you));
        state = sm.getNextState(state, Collections.singletonList(move("proceed")));
        System.out.println(state);
        System.out.println("TERMINAL: " + sm.isTerminal(state));
        System.out.println("Goal should be 100: " + sm.getGoal(state, you));
        System.out.println(sm.getGoals(state));
    }

    // for test purpose
    private static Move move(String description) {
        String[] parts = description.split(" ");
        GdlConstant head = GdlPool.getConstant(parts[0]);
        if(parts.length == 1)
            return new Move(head);
        List<GdlTerm> body = new ArrayList<GdlTerm>();
        for(int i = 1; i < parts.length; i++) {
            body.add(GdlPool.getConstant(parts[i]));
        }
        return new Move(GdlPool.getFunction(head, body));
    }
}