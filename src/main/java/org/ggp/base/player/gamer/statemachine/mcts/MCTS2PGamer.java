package org.ggp.base.player.gamer.statemachine.mcts;

import org.ggp.base.player.gamer.event.GamerSelectedMoveEvent;
import org.ggp.base.player.gamer.exception.GamePreviewException;
import org.ggp.base.player.gamer.statemachine.GameAnalyser;
import org.ggp.base.player.gamer.statemachine.StateMachineGamer;
import org.ggp.base.player.gamer.statemachine.mcts.tree.MonteCarlo2PGTree;
import org.ggp.base.util.game.Game;
import org.ggp.base.util.logging.GamerLogger;
import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.statemachine.StateMachine;
import org.ggp.base.util.statemachine.cache.CachedStateMachine;
import org.ggp.base.util.statemachine.exceptions.GoalDefinitionException;
import org.ggp.base.util.statemachine.exceptions.MoveDefinitionException;
import org.ggp.base.util.statemachine.exceptions.TransitionDefinitionException;
import org.ggp.base.util.statemachine.implementation.prover.ProverStateMachine;

import java.util.List;

/**
 * Peara - 2/1/16.
 * Basically a MTCS gamer but can only play 2-player games
 * Advantage over general MCTS gamer in 2-player games: memory, etc.
 */
public class MCTS2PGamer extends StateMachineGamer {
    MonteCarlo2PGTree searcher;
    GameAnalyser analyser;

    @Override
    public StateMachine getInitialStateMachine() {
        // TODO this will return a PropnetStateMachine later
        return new CachedStateMachine(new ProverStateMachine());
        // return new CachedStateMachine(new PropNetStateMachine());
    }

    @Override
    public void stateMachineMetaGame(long timeout) throws TransitionDefinitionException, MoveDefinitionException, GoalDefinitionException {
        analyser = new GameAnalyser(getStateMachine());
        GamerLogger.log("MCTS2PGamer", "this is a two-player game: " + String.valueOf(analyser.is2PGame()));
        GamerLogger.log("MCTS2PGamer", "this is a two-player turn-based game: " + String.valueOf(analyser.isTurnBase2PGame()));
        searcher = new MonteCarlo2PGTree(0.87, getStateMachine(), getMatch());
    }

    @Override
    public Move stateMachineSelectMove(long timeout) throws TransitionDefinitionException, MoveDefinitionException, GoalDefinitionException {
        long start = System.currentTimeMillis();
        long finishBy = timeout - 1000;

        Role active_role;
        // determine current active role
        if (getMatch().getMoveHistory().size() % 2 == 0) {
            // first player turn
            active_role = analyser.getFirstPlayer();
        } else {
            // second player turn
            active_role = analyser.getSecondPlayer();
        }

        Move selection = searcher.selectBestMove(getCurrentState(), getRole(), finishBy, active_role);

        long stop = System.currentTimeMillis();

        List<Move> moves = getStateMachine().getLegalMoves(getCurrentState(), getRole());
        notifyObservers(new GamerSelectedMoveEvent(moves, selection, stop - start));
        return selection;
    }

    @Override
    public void stateMachineStop() {
        //TODO nothing for now
    }

    @Override
    public void stateMachineAbort() {
        //TODO nothing for now
    }

    @Override
    public void preview(Game g, long timeout) throws GamePreviewException {
        //TODO more later
    }

    @Override
    public String getName() {
        return "Kaao-kuro MCTS2P";
    }
}