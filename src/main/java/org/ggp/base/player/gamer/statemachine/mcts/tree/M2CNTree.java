package org.ggp.base.player.gamer.statemachine.mcts.tree;

import org.ggp.base.util.logging.GamerLogger;
import org.ggp.base.util.match.Match;
import org.ggp.base.util.statemachine.MachineState;
import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.statemachine.StateMachine;
import org.ggp.base.util.statemachine.exceptions.GoalDefinitionException;
import org.ggp.base.util.statemachine.exceptions.MoveDefinitionException;
import org.ggp.base.util.statemachine.exceptions.TransitionDefinitionException;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Peara - 2/6/16.
 */
public class M2CNTree extends MonteCarlo2PGTree {
    final private int oo = 10000;
    int m_depth;

    public M2CNTree(int depth, double exp_coeff, StateMachine machine, Match match) {
        m_depth = 4;
        m_exp_coeff = exp_coeff;
        theMachine = machine;
        theMatch = match;
    }

    /**
     * Calculate Conspiracy Numbers of a given node to decrease its value to the target value (winning percentage)
     * @param node
     * @param target
     * @param depth - if depth is 0, leaf node
     * @return
     */
    protected int calculate_cn_dec(MCNode node, double target, int depth) {
        // terminal node
        if (theMachine.isTerminal(node.state)) {
            if (node.getWinRate() >= target) {
                return oo;
            }
            return 0;
        }
        // leaf node
        if (depth == 0 || node.m_move_list == null) {
            if (node.getWinRate() >= target) {
                return 1;
            }
            return 0;
        }
        int res = 0;
        for (Map.Entry<Move, MCNode> entry : node.stateInfo.entrySet()) {
            res += calculate_cn_inc(entry.getValue(), 1-target, depth-1);
        }
        return res;
    }

    /**
     * Calculate Conspiracy Numbers of a given node to increase its value to the target value (winning percentage)
     * @param node
     * @param target
     * @param depth - if depth is 0, leaf node
     */
    protected int calculate_cn_inc(MCNode node, double target, int depth) {
        if (node.getFixedWinrate() > target) {
            return 0;
        }
        // terminal node
        if (theMachine.isTerminal(node.state)) {
            return oo;
        }
        // leaf node
        if (depth == 0 || node.m_move_list == null) {
            return 1;
        }

        int res = oo;
        for (Map.Entry<Move, MCNode> entry : node.stateInfo.entrySet()) {
            res = Math.min(res, calculate_cn_dec(entry.getValue(), 1-target, depth-1));
        }

        return res;
    }

    /**
     * Use CAEV to modify the winrate of node
     * @param node
     * @param depth
     */
    protected void CAEV(MCNode node, int depth) {
        // get 1/CN(v)
        double CNs[] = new double[101];
        double temp;
        for (int i=0; i<101; i++) {
            if (node.getFixedWinrate() > i) {
                temp = calculate_cn_dec(node, i, depth);
                if (temp == 0) {
                    CNs[i] = 1;
                } else {
                    CNs[i] = temp;
                }
            } else {
                temp = calculate_cn_inc(node, i, depth);
                if (temp == 0) {
                    CNs[i] = 1;
                } else {
                    CNs[i] = temp;
                }
            }
        }

        // integral
        double norm_val = (1.0/CNs[0] + 1.0/CNs[100])/2;
        for (int i=1; i<100; i++) {
            norm_val += 1.0/CNs[i];
        }

        double res = 0;
        double a, b, cn_1, cn_2;
        cn_2 = CNs[0];
        for (int i=0; i<100; i++) {
            cn_1 = cn_2;
            cn_2 = CNs[i];
            a = cn_2 - cn_1;
            b = cn_2 - a*i;

            temp = (a != 0) ? (Math.log(cn_2) - Math.log(cn_1))/a : 1.0/cn_1;

            if (a != 0 && b != 0) {
                temp = 1.0/a + b*(Math.log(cn_1) - Math.log(cn_2))/Math.pow(a,2);
            } else {
                if (a == 0) {
                    temp = (2.0*i-1)/(2*b);
                } else {
                    temp = 1.0/a;
                }
            }
            res += temp;
        }


        node.setWinrate(res/norm_val);
    }

    @Override
    public Move selectBestMove(MachineState state, Role role, long timeout, Role active_role) throws MoveDefinitionException, TransitionDefinitionException, GoalDefinitionException {
        // update to the current game state
        theState = state;
        /*if (cache.contain(theState, lastMoves)) {
            root = cache.get(theState, lastMoves);
        } else {
*/
        root = new MCNode(theState, active_role);
//            cache.put(theState, lastMoves, root);
        root.expand();
//        GamerLogger.log("MCTree2PG", "number of moves: " + root.m_move_list.size());
//        GamerLogger.log("MCTree2PG", "current role: " + role.toString());
//        GamerLogger.log("MCTree2PG", "active role: " + active_role.toString());
//        }

        // find best move
        MCNode cur;
        MachineState finalState;
        List<Integer> scores;
        List<MCNode> nodeHist;

        int simulationCount = 0;
        int step_count;
        while (System.currentTimeMillis() < timeout) {
            nodeHist = new ArrayList<MCNode>();
            cur = root;
            nodeHist.add(cur);
            // selection
            do {
                cur = cur.stateInfo.get(cur.selectMove());
                /*
                if (!cache.contain(tempState, selectedMoves)) {
                    cur = new MCNode(tempState);
//                    cache.put(cur.state, selectedMoves, cur);
                    cur.init();
                } else {
                    assert cur != null;
//                    cur = cache.get(tempState, selectedMoves);
                }
                */

                nodeHist.add(cur);
            } while (cur.m_move_list != null && !theMachine.isTerminal(cur.state));

            // expansion
            // if current node is visited more than limit
            // if less than minimum depth, expand anyway
            if (nodeHist.size() <= m_depth || cur.visitCount > 10) {
                cur.expand();
            }

            // simulation
            finalState = theMachine.performDepthCharge(cur.state, null);

            // backpropagation
            // TODO: check
            scores = theMachine.getGoals(finalState);
            for (int i=0; i<nodeHist.size(); i++) {
                nodeHist.get(i).update(scores);
            }
            // nodeHist.get(nodeHist.size()-1).visitCount ++;
            simulationCount ++;
        }

        root.fixWinrate(m_depth);

        // DEBUG
        DecimalFormat formatter = new DecimalFormat("#.0###");
        String s = "Before CAEV: ";
        for (Map.Entry<Move, MCNode> entry : root.stateInfo.entrySet()) {
            s += "[" + entry.getKey().toString() + " --- " + formatter.format(entry.getValue().getFixedWinrate()) + "] ";
        }
        GamerLogger.log("M2CNTree", s);

        // applying CAEV
        for (Map.Entry<Move, MCNode> entry : root.stateInfo.entrySet()) {
            CAEV(entry.getValue(), m_depth-1);
        }

        // select best move based on modified winrates
//        s = "After CAEV: ";
//        for (Map.Entry<Move, MCNode> entry : root.stateInfo.entrySet()) {
//                s += String.valueOf(entry.getValue().getFixedWinrate()) + " ";
//        }
//        GamerLogger.log("M2CNTree", s);
        Move selected_move = root.selectBestMove(role);

        GamerLogger.log("M2CNTree", "number of simulations: " + simulationCount);
        GamerLogger.log("M2CNTree", "selected move: " + selected_move.toString());
        return selected_move;
    }
}
