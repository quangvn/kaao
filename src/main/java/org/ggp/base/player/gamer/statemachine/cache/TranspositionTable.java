package org.ggp.base.player.gamer.statemachine.cache;


import org.ggp.base.util.logging.GamerLogger;
import org.ggp.base.util.statemachine.MachineState;
import org.ggp.base.util.statemachine.Move;

import java.util.HashMap;
import java.util.List;

/**
 * User: Peara
 * Date: 1/24/2015
 * Time: 11:07 PM
 */
public class TranspositionTable<V> {
    HashMap<MachineState, HashMap<List<Move>, V>> cache;

    public TranspositionTable() {
        cache = new HashMap<MachineState, HashMap<List<Move>, V>>();
    }

    // return null or currently associated value
    public V put(MachineState state, List<Move> moves, V v) {
        // totally new entry
        if (!cache.containsKey(state)) {
            HashMap<List<Move>, V> t = new HashMap<List<Move>, V>();
            cache.put(state, t);
            return t.put(moves, v); // should be null
        } else {
            // same state, new moves
            if (!cache.get(state).containsKey(moves)) {
                return cache.get(state).put(moves, v); // should be null
            } else {
                /// !!! DUPLICATE ENTRY !!!
                // this is the case where the cache already contains an entry with same keys
                // should never reach this
                GamerLogger.log("TranspositionTable", "Duplicate entry");
                return cache.get(state).get(moves);
            }
        }
    }

    public int size() {
        return cache.size();
    }

    public V get(MachineState state, List<Move> moves) {
        if (!cache.containsKey(state)) {
            return null;
        }
        if (!cache.get(state).containsKey(moves)) {
            return null;
        }
        return cache.get(state).get(moves);
    }

    public boolean contain(MachineState state, List<Move> moves) {
        if (!cache.containsKey(state)) {
            return false;
        }
        if (!cache.get(state).containsKey(moves)) {
            return false;
        }
        return true;
    }
}
