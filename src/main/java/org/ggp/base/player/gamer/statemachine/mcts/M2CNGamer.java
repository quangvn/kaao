package org.ggp.base.player.gamer.statemachine.mcts;

import org.ggp.base.player.gamer.event.GamerSelectedMoveEvent;
import org.ggp.base.player.gamer.exception.GamePreviewException;
import org.ggp.base.player.gamer.statemachine.GameAnalyser;
import org.ggp.base.player.gamer.statemachine.StateMachineGamer;
import org.ggp.base.player.gamer.statemachine.mcts.tree.M2CNTree;
import org.ggp.base.player.gamer.statemachine.mcts.tree.MonteCarlo2PGTree;
import org.ggp.base.util.game.Game;
import org.ggp.base.util.logging.GamerLogger;
import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.statemachine.StateMachine;
import org.ggp.base.util.statemachine.cache.CachedStateMachine;
import org.ggp.base.util.statemachine.exceptions.GoalDefinitionException;
import org.ggp.base.util.statemachine.exceptions.MoveDefinitionException;
import org.ggp.base.util.statemachine.exceptions.TransitionDefinitionException;
import org.ggp.base.util.statemachine.implementation.prover.ProverStateMachine;

import java.util.List;

/**
 * This gamer is extended from MCTS2PGamer.
 * The difference is it will expand the tree for at least a given depth then applying CN for the given depth tree.
 * Kaao-chairo
 *
 * Peara - 2/6/16.
 */
public class M2CNGamer extends StateMachineGamer {

    M2CNTree searcher;
    GameAnalyser analyser;

    @Override
    public StateMachine getInitialStateMachine() {
        // TODO this will return a PropnetStateMachine later
        return new CachedStateMachine(new ProverStateMachine());
        // return new CachedStateMachine(new PropNetStateMachine());
    }

    @Override
    public void stateMachineMetaGame(long timeout) throws TransitionDefinitionException, MoveDefinitionException, GoalDefinitionException {
        analyser = new GameAnalyser(getStateMachine());
        GamerLogger.log("M2CNGamer", "this is a two-player game: " + String.valueOf(analyser.is2PGame()));
        GamerLogger.log("M2CNGamer", "this is a two-player turn-based game: " + String.valueOf(analyser.isTurnBase2PGame()));
        searcher = new M2CNTree(5, 0.87, getStateMachine(), getMatch());
    }

    @Override
    public Move stateMachineSelectMove(long timeout) throws TransitionDefinitionException, MoveDefinitionException, GoalDefinitionException {
        long start = System.currentTimeMillis();
        long finishBy = timeout - 1000;

        Role active_role;
        // determine current active role
        if (getMatch().getMoveHistory().size() % 2 == 0) {
            // first player turn
            active_role = analyser.getFirstPlayer();
        } else {
            // second player turn
            active_role = analyser.getSecondPlayer();
        }

        Move selection = searcher.selectBestMove(getCurrentState(), getRole(), finishBy, active_role);

        long stop = System.currentTimeMillis();

        List<Move> moves = getStateMachine().getLegalMoves(getCurrentState(), getRole());
        notifyObservers(new GamerSelectedMoveEvent(moves, selection, stop - start));
        return selection;
    }

    @Override
    public void stateMachineStop() {
        //TODO nothing for now
    }

    @Override
    public void stateMachineAbort() {
        //TODO nothing for now
    }

    @Override
    public void preview(Game g, long timeout) throws GamePreviewException {
        //TODO more later
    }

    @Override
    public String getName() {
        return "Kaao-chairo M2CN";
    }
}
