package org.ggp.base.player.gamer.statemachine.mcts.tree;

import org.ggp.base.player.gamer.statemachine.cache.TranspositionTable;
import org.ggp.base.util.gdl.grammar.GdlTerm;
import org.ggp.base.util.logging.GamerLogger;
import org.ggp.base.util.match.Match;
import org.ggp.base.util.statemachine.MachineState;
import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.statemachine.StateMachine;
import org.ggp.base.util.statemachine.exceptions.GoalDefinitionException;
import org.ggp.base.util.statemachine.exceptions.MoveDefinitionException;
import org.ggp.base.util.statemachine.exceptions.TransitionDefinitionException;

import java.util.*;

/**
 * User: Peara
 * Date: 3/22/2015
 * Time: 10:56 PM
 */
public class MonteCarloTree2 {
    private class MoveStats {
        public int selectCount;
        public int totalScore;

        public MoveStats() {
            selectCount = 0;
            totalScore = 0;
        }

        public double getWinRate() {
            if (selectCount > 0) {
                return (double)totalScore/selectCount;
            }
            return 0;
        }
    }
    private class Node {
        MachineState state;
        int visitCount;
        HashMap<Role, HashMap<Move, MoveStats>> stateInfo = null;
        public Queue<List<Move>> newMovesQueue;

        public Node(MachineState _state) {
            state = _state;
            visitCount = 0;
        }

        private void generateMovesList(int index, List<Move> temp, Queue<List<Move>> queue)
                throws MoveDefinitionException {
            if (theMachine.isTerminal(this.state)) {
                return;
            }
            if (index==roles.size()) {
                queue.add(new ArrayList<Move>(temp));
                return;
            }
            for (Move m : theMachine.getLegalMoves(this.state, roles.get(index))) {
                temp.add(m);
                generateMovesList(index+1, temp, queue);
                temp.remove(index);
            }
        }

        public void init()
                throws MoveDefinitionException {
            stateInfo = new HashMap<Role, HashMap<Move, MoveStats>>();
            // if this is a terminal state, no need for any moves
            if (theMachine.isTerminal(this.state)) {
                return;
            }
            newMovesQueue = new ArrayDeque<List<Move>>();
            generateMovesList(0, new ArrayList<Move>(), newMovesQueue);
            List<Move> moves;
            Role r;
            HashMap<Move, MoveStats> h;
            for (int i=0; i<roles.size(); i++) {
                r = roles.get(i);
                h = new HashMap<Move, MoveStats>();
                stateInfo.put(r, h);
                moves = theMachine.getLegalMoves(state, r);
                for (int j=0; j<moves.size(); j++) {
                    h.put(moves.get(j), new MoveStats());
                }
            }
        }

        public double getUCB(Role role, Move move) {
            MoveStats stats = stateInfo.get(role).get(move);
            return ((double)stats.totalScore/stats.selectCount) + expCoeff*Math.sqrt(Math.log(visitCount)/stats.selectCount);
        }

        public Move selectMove(Role role) {
            HashMap<Move, MoveStats> rmove = stateInfo.get(role);
            double maxucb = 0;
            Move res = null;
            for (Map.Entry<Move, MoveStats> entry : rmove.entrySet()) {
                if (getUCB(role, entry.getKey()) > maxucb) {
                    maxucb = getUCB(role, entry.getKey());
                    res = entry.getKey();
                }
            }
            return res;
        }

        // in roles order
        public List<Move> selectMoves() {
            List<Move> res;
            if (! newMovesQueue.isEmpty()) {
                res = newMovesQueue.poll();
                return res;
            }
            res = new ArrayList<Move>();
            for (int i=0; i<roles.size(); i++) {
                res.add(selectMove(roles.get(i)));
            }
            return res;
        }

        // scores and moves list both have the same order as the roles list
        public void update(List<Integer> scores, List<Move> moves) {
            MoveStats stats;
            for (int i=0; i<roles.size(); i++) {
                stats = stateInfo.get(roles.get(i)).get(moves.get(i));
                stats.selectCount ++;
                stats.totalScore += scores.get(i);
            }
            this.visitCount ++;
        }

        public Move selectBestMove(Role role) {
            HashMap<Move, MoveStats> rmove = stateInfo.get(role);
            double maxwr = 0;
            Move res = null;
            for (Map.Entry<Move, MoveStats> entry : rmove.entrySet()) {
                if (entry.getValue().getWinRate() > maxwr) {
                    maxwr = entry.getValue().getWinRate();
                    res = entry.getKey();
                }
            }
            return res;
        }

        // deviation of winrate of a role
        // deviation = sum_all_children((winrate(child)-winrate)*visit_count(child))/visit_count
        //                               win_count(child)-winrate*visit_count(child)
        public double getDeviation(Role role) {
            double res;
            double sum = 0;
            double thisWinRate = 0;
            HashMap<Move, MoveStats> rmove = stateInfo.get(role);
            if (rmove == null) {
                GamerLogger.log("DEVIATION", "this should be terminal: " + theMachine.isTerminal(state));
                return 0;
            }
            GamerLogger.log("DEVIATION", "number of children: " + rmove.size());

            // calculate mean winrate
            for (Map.Entry<Move, MoveStats> entry : rmove.entrySet()) {
                thisWinRate += entry.getValue().totalScore;
            }
            thisWinRate /= visitCount;
            GamerLogger.log("DEVIATION", "mean: " + thisWinRate);

            // calculate deviation
            for (Map.Entry<Move, MoveStats> entry : rmove.entrySet()) {
                sum += Math.pow((entry.getValue().getWinRate() - thisWinRate),2)*entry.getValue().selectCount;
            }
            GamerLogger.log("DEVIATION", "sum: " + sum);
            res = Math.sqrt(sum / visitCount);

            return res;
        }

        /**
         * select best move with the less deviation in its children
         * @param role
         * @return
         */
        public Move selectBestMove2(Role role)
                throws TransitionDefinitionException {
            HashMap<Move, MoveStats> rmove = stateInfo.get(role);
            double maxwr = 0;
            Move res = null;
            for (Map.Entry<Move, MoveStats> entry : rmove.entrySet()) {
                if (entry.getValue().getWinRate() > maxwr) {
                    maxwr = entry.getValue().getWinRate();
                }
            }

            double mindev = 100000;
            List<Move> moves = this.selectMoves();
            Node temp;
            int pos = theMachine.getRoleIndices().get(role);
            double tdev;
            for (Map.Entry<Move, MoveStats> entry : rmove.entrySet()) {
                if (entry.getValue().getWinRate() >= maxwr*0.9) {
                    moves.set(pos, entry.getKey());
                    temp = cache.get(theMachine.getNextState(state, moves), moves);

                    tdev = temp.getDeviation(role);
                    GamerLogger.log("DEVIATION", String.valueOf(tdev));
                    if (tdev < mindev) {
                        mindev = tdev;
                        res = entry.getKey();
                    }
                }
            }

            return res;
        }
    }

    long timeout;
    double expCoeff;
    StateMachine theMachine;
    MachineState theState;
    Match theMatch;
    List<Role> roles;
    List<Move> lastMoves;
    Node root;
    TranspositionTable<Node> cache;

    public MonteCarloTree2(double _expCoeff, StateMachine _machine, Match _match)
            throws MoveDefinitionException {
        expCoeff = _expCoeff;
        theMachine = _machine;
        theMatch = _match;
        roles = theMachine.getRoles();
        cache = new TranspositionTable<Node>();
    }

    public Move selectBestMove(MachineState state, Role role, long timeout)
            throws MoveDefinitionException, TransitionDefinitionException, GoalDefinitionException {

        // update to the current game state
        theState = state;
        lastMoves = new ArrayList<Move>();
        List<GdlTerm> lastMovesInGDL = theMatch.getMostRecentMoves();
        if (lastMovesInGDL != null) {
            for (GdlTerm sentence : lastMovesInGDL) {
                lastMoves.add(theMachine.getMoveFromTerm(sentence));
            }
        }
        if (cache.contain(theState, lastMoves)) {
            root = cache.get(theState, lastMoves);
        } else {
            root = new Node(theState);
            cache.put(theState, lastMoves, root);
            root.init();
            GamerLogger.log("MonteCarlo", "number of moves: " + root.newMovesQueue.size());
        }

        // find best move
        Node cur;
        int[] depth = new int[1];
        MachineState finalState;
        List<Integer> scores;
        List<Node> nodeHist;
        List<List<Move>> movesHist;

        int simulationCount = 0;
        while (System.currentTimeMillis() < timeout) {
            nodeHist = new ArrayList<Node>();
            movesHist = new ArrayList<List<Move>>();
            cur = root;
            nodeHist.add(cur);
            movesHist.add(lastMoves);
            // selection
            List<Move> selectedMoves;
            MachineState tempState;
            do {
                selectedMoves = cur.selectMoves();
                // GamerLogger.log("MonteCarlo", "selected moves: " + selectedMoves.toString());
                tempState = theMachine.getNextState(cur.state, selectedMoves);
                // expansion
                if (!cache.contain(tempState, selectedMoves)) {
                    cur = new Node(tempState);
                    cache.put(cur.state, selectedMoves, cur);
                    cur.init();
                } else {
                    assert cur != null;
                    cur = cache.get(tempState, selectedMoves);
                }
                nodeHist.add(cur);
                movesHist.add(selectedMoves);
            } while (cur.visitCount > 0 && !theMachine.isTerminal(cur.state));

            // simulation
            finalState = theMachine.performDepthCharge(cur.state, depth);

            // backpropagation
            scores = theMachine.getGoals(finalState);
            for (int i=1; i<nodeHist.size(); i++) {
                nodeHist.get(i-1).update(scores, movesHist.get(i));
            }
            nodeHist.get(nodeHist.size()-1).visitCount ++;
            simulationCount ++;
        }

        GamerLogger.log("MonteCarloTreeSearch", "Number of simulations: " + simulationCount);
        return root.selectBestMove2(role);
    }
}