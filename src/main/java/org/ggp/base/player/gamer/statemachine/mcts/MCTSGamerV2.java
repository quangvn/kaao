package org.ggp.base.player.gamer.statemachine.mcts;

import org.ggp.base.player.gamer.event.GamerSelectedMoveEvent;
import org.ggp.base.player.gamer.exception.GamePreviewException;
import org.ggp.base.player.gamer.statemachine.StateMachineGamer;
import org.ggp.base.player.gamer.statemachine.mcts.tree.MonteCarloTree;
import org.ggp.base.player.gamer.statemachine.mcts.tree.MonteCarloTree2;
import org.ggp.base.util.game.Game;
import org.ggp.base.util.logging.GamerLogger;
import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.StateMachine;
import org.ggp.base.util.statemachine.cache.CachedStateMachine;
import org.ggp.base.util.statemachine.exceptions.GoalDefinitionException;
import org.ggp.base.util.statemachine.exceptions.MoveDefinitionException;
import org.ggp.base.util.statemachine.exceptions.TransitionDefinitionException;
import org.ggp.base.util.statemachine.implementation.propnet.PropNetStateMachine;
import org.ggp.base.util.statemachine.implementation.prover.ProverStateMachine;

import java.util.List;

/**
 * User: Peara
 * Date: 3/22/2015
 * Time: 10:55 PM
 */
public class MCTSGamerV2 extends StateMachineGamer{
    MonteCarloTree searcher;

    public StateMachine getInitialStateMachine() {
        return new CachedStateMachine(new PropNetStateMachine());
    }

    @Override
    public void stateMachineMetaGame(long timeout) throws TransitionDefinitionException, MoveDefinitionException, GoalDefinitionException {
        searcher = new MonteCarloTree(0.87, getStateMachine(), getMatch());
    }

    @Override
    public Move stateMachineSelectMove(long timeout) throws TransitionDefinitionException, MoveDefinitionException, GoalDefinitionException {
        long start = System.currentTimeMillis();
        long finishBy = timeout - 1000;

        List<Move> moves = getStateMachine().getLegalMoves(getCurrentState(), getRole());
        GamerLogger.log("PropNet", "Number of moves: " + moves.size());
        GamerLogger.log("PropNet", "Active role: " + getRole().getName());
        GamerLogger.log("PropNet", "Number of roles: " + getStateMachine().getRoles().size());
        Move selection = searcher.selectBestMove(getCurrentState(), getRole(), finishBy);

        long stop = System.currentTimeMillis();

        notifyObservers(new GamerSelectedMoveEvent(moves, selection, stop - start));
        return selection;
    }

    @Override
    public void stateMachineStop() {
        //TODO nothing for now
    }

    @Override
    public void stateMachineAbort() {
        //TODO nothing for now
    }

    @Override
    public void preview(Game g, long timeout) throws GamePreviewException {
        //TODO later
    }

    @Override
    public String getName() {
        return "Kaao-momoiro MCTS-propnet";
    }
}
