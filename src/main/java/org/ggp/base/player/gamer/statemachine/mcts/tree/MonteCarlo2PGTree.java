package org.ggp.base.player.gamer.statemachine.mcts.tree;

import org.ggp.base.util.logging.GamerLogger;
import org.ggp.base.util.match.Match;
import org.ggp.base.util.statemachine.MachineState;
import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.statemachine.StateMachine;
import org.ggp.base.util.statemachine.exceptions.GoalDefinitionException;
import org.ggp.base.util.statemachine.exceptions.MoveDefinitionException;
import org.ggp.base.util.statemachine.exceptions.TransitionDefinitionException;

import java.util.*;

/**
 * Peara - 1/25/16.
 * <p/>
 * This class is the same as MonteCarloTree, but specialized for 2-player games.
 * Assumed that at any position, only 1 player has real choice.
 * These other will just select the dummy move.
 */

public class MonteCarlo2PGTree {
    long timeout;
    double m_exp_coeff;
    StateMachine theMachine;
    MachineState theState;
    Match theMatch;
    List<Role> roles;
    int m_turn; // index of the active role (0 or 1)
    Role m_first_player, m_second_player;
    MCNode root;
//    TranspositionTable<MCNode> cache;

    public void setFirstPlayer(Role r) {
        m_first_player = r;
    }

    public void setSecondPlayer(Role r) {
        m_second_player = r;
    }

    public void setTurn(int turn) {
        m_turn = turn;
    }

    /**
     * Tree-structure class
     */
    protected class MCNode {
        MachineState state;
        int visitCount;
        int selectCount;
        int totalScore;
        double m_win_rate;
        Role m_active_role; // the role has control of current state
        int m_active_role_id; // id of the active role
        Role m_passive_role;
        HashMap<Move, MCNode> stateInfo = null;
        public List<Move> m_move_list = null;

        public MCNode(MachineState _state, Role active_role) {
            state = _state;
            visitCount = 0;
            selectCount = 0;
            totalScore = 0;
            m_active_role = active_role;
            if (m_active_role.equals(theMachine.getRoles().get(0))) {
                m_active_role_id = 0;
                m_passive_role = theMachine.getRoles().get(1);
            } else {
                m_active_role_id = 1;
                m_passive_role = theMachine.getRoles().get(0);
            }
        }

        /**
         * set the fixed winrate value
         * @param value
         */
        public void setWinrate(double value) {
            m_win_rate = value;
        }

        /**
         * get the fixed winrate value
         * @return
         */
        public double getFixedWinrate() {
            return m_win_rate;
        }

        /**
         * set the current winrate to the fixed winrate
         * recursively call child nodes
         * @param depth maximum distance from current node
         */
        public void fixWinrate(int depth) {
            m_win_rate = getWinRate();
            if (depth > 0) {
                if (m_move_list != null) {
                    for (Map.Entry<Move, MCNode> entry : stateInfo.entrySet()) {
                        entry.getValue().fixWinrate(depth - 1);
                    }
                }
            }
        }

        /**
         * get winrate based on simulation
         * @return
         */
        public double getWinRate() {
            if (selectCount > 0) {
                return (double) totalScore / selectCount;
            }
            return 0;
        }

        /**
         * Expand this node
         * @throws MoveDefinitionException
         */
        protected void expand()
                throws MoveDefinitionException {
            stateInfo = new HashMap<Move, MCNode>();
            // if this is a terminal state, no need for any moves
            if (theMachine.isTerminal(this.state)) {
                return;
            }
            m_move_list = theMachine.getLegalMoves(state, m_active_role);
            Move noop = theMachine.getLegalMoves(state, m_passive_role).get(0);

            List<Move> moves = new ArrayList<>();
            for (Move move : m_move_list) {
                // generate move combination
                moves.clear();
                if (m_active_role_id == 0) {
                    moves.add(move);
                    moves.add(noop);
                } else {
                    moves.add(noop);
                    moves.add(move);
                }
                try {
                    stateInfo.put(move, new MCNode(theMachine.getNextState(state, moves), m_passive_role));
                } catch (TransitionDefinitionException e) {
                    GamerLogger.logError("MCTree2PG", "Expansion error: " + e.getMessage());
                }
            }
        }

        /**
         * Get UCB of current move
         * @param move
         * @return
         */
        public double getUCB(Move move) {
            MCNode node = stateInfo.get(move);
            return ((double) node.totalScore / node.selectCount) + m_exp_coeff * Math.sqrt(Math.log(visitCount) / node.selectCount);
        }

        /**
         * Select a move with maximum UCB value
         * @return
         */
        public Move selectMove() {
            double maxucb = 0;
            Move res = null;
            for (Map.Entry<Move, MCNode> entry : stateInfo.entrySet()) {
                // TODO: optimize
                // immediately return an unused move
                if (entry.getValue().visitCount == 0) {
                    return entry.getKey();
                }
                if (getUCB(entry.getKey()) > maxucb) {
                    maxucb = getUCB(entry.getKey());
                    res = entry.getKey();
                }
            }
            return res;
        }

        /**
         * Generate move combination based on UCB
         * @return
         */
        public List<Move> selectMoves() throws MoveDefinitionException {
            List<Move> res;
            // TODO: if there is unused moves
            /*
            if (!m_move_list.isEmpty()) {
                res = m_move_list.poll();
                return res;
            }
            */

            // choose move by UCB
            res = new ArrayList<Move>();
            if (m_active_role_id == 0) {
                res.add(selectMove());
                res.add(theMachine.getLegalMoves(state, m_passive_role).get(0));
            } else {
                res.add(theMachine.getLegalMoves(state, m_passive_role).get(0));
                res.add(selectMove());
            }
            return res;
        }

        /**
         * Update score of node
         * @param scores
         */
        // scores and moves list both have the same order as the roles list
        public void update(List<Integer> scores) {
            selectCount ++;
            totalScore += scores.get(m_active_role_id);
            this.visitCount++;
        }

        // deviation of winrate of a role
        // deviation = sum_all_children((winrate(child)-winrate)*visit_count(child))/visit_count
        //                               win_count(child)-winrate*visit_count(child)
/*
        public double getDeviation(Role role) {
            double res;
            double sum = 0;
            double thisWinRate = 0;
            HashMap<Move, MoveStats> rmove = stateInfo.get(role);

            // calculate mean winrate
            for (Map.Entry<Move, MoveStats> entry : rmove.entrySet()) {
                thisWinRate += entry.getValue().totalScore;
            }
            thisWinRate /= visitCount;

            // calculate deviation
            for (Map.Entry<Move, MoveStats> entry : rmove.entrySet()) {
                sum += Math.pow((entry.getValue().totalScore - thisWinRate), 2) * entry.getValue().selectCount;
            }
            res = Math.sqrt(sum / visitCount);

            return res;
        }
*/

        /**
         * Best move selection
         * @param role
         * @return
         */
        public Move selectBestMove(Role role) throws MoveDefinitionException {
            if (role.equals(m_passive_role)) {
                return theMachine.getLegalMoves(state, role).get(0);
            }

            double min_op_winrate = 100;
            Move res = null;
            for (Map.Entry<Move, MCNode> entry : stateInfo.entrySet()) {
                if (entry.getValue().getWinRate() < min_op_winrate) {
                    min_op_winrate = entry.getValue().getWinRate();
                    res = entry.getKey();
                }
            }
            return res;
        }

        /**
         * Best move selection based on fixed win rates
         * @param role
         * @return
         */
        public Move selectBestMoveF(Role role) throws MoveDefinitionException {
            if (role.equals(m_passive_role)) {
                return theMachine.getLegalMoves(state, role).get(0);
            }

            double min_op_winrate = 100;
            Move res = null;
            for (Map.Entry<Move, MCNode> entry : stateInfo.entrySet()) {
                if (entry.getValue().getFixedWinrate() < min_op_winrate) {
                    min_op_winrate = entry.getValue().getFixedWinrate();
                    res = entry.getKey();
                }
            }
            return res;
        }
    }

    public MonteCarlo2PGTree() {}

    public MonteCarlo2PGTree(double _expCoeff, StateMachine _machine, Match _match)
            throws MoveDefinitionException {
        m_exp_coeff = _expCoeff;
        theMachine = _machine;
        theMatch = _match;
        roles = theMachine.getRoles();
        // cache = new TranspositionTable<MCNode>();
    }

    public Move selectBestMove(MachineState state, Role role, long timeout, Role active_role)
            throws MoveDefinitionException, TransitionDefinitionException, GoalDefinitionException {

        // update to the current game state
        theState = state;
        /*if (cache.contain(theState, lastMoves)) {
            root = cache.get(theState, lastMoves);
        } else {
*/
        root = new MCNode(theState, active_role);
//            cache.put(theState, lastMoves, root);
        root.expand();
//        GamerLogger.log("MCTree2PG", "number of moves: " + root.m_move_list.size());
//        GamerLogger.log("MCTree2PG", "current role: " + role.toString());
//        GamerLogger.log("MCTree2PG", "active role: " + active_role.toString());
//        }

        // find best move
        MCNode cur;
        int[] depth = new int[1];
        MachineState finalState;
        List<Integer> scores;
        List<MCNode> nodeHist;

        int simulationCount = 0;
        while (System.currentTimeMillis() < timeout) {
            nodeHist = new ArrayList<MCNode>();
            cur = root;
            nodeHist.add(cur);
            // selection
            do {
                // selectedMoves = cur.selectMoves();
                // GamerLogger.log("MonteCarlo", "selected moves: " + selectedMoves.toString());
                // tempState = theMachine.getNextState(cur.state, selectedMoves);

                cur = cur.stateInfo.get(cur.selectMove());

                /*
                if (!cache.contain(tempState, selectedMoves)) {
                    cur = new MCNode(tempState);
//                    cache.put(cur.state, selectedMoves, cur);
                    cur.init();
                } else {
                    assert cur != null;
//                    cur = cache.get(tempState, selectedMoves);
                }
                */

                nodeHist.add(cur);
            } while (cur.m_move_list != null && !theMachine.isTerminal(cur.state));

            // expansion
            // if current node is visited more than limit
            if (cur.visitCount > 10) {
                cur.expand();
            }

            // simulation
            finalState = theMachine.performDepthCharge(cur.state, depth);

            // backpropagation
            // TODO: check
            scores = theMachine.getGoals(finalState);
            for (int i=0; i<nodeHist.size(); i++) {
                nodeHist.get(i).update(scores);
            }
            // nodeHist.get(nodeHist.size()-1).visitCount ++;
            simulationCount ++;
        }

        Move selected_move = root.selectBestMove(role);

        GamerLogger.log("MC2PGTree", "number of simulations: " + simulationCount);
        GamerLogger.log("MC2PGTree", "selected move: " + selected_move.toString());
        return selected_move;
    }
}
