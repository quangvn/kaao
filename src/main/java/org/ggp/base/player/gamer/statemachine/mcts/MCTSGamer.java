package org.ggp.base.player.gamer.statemachine.mcts;

import org.ggp.base.player.gamer.event.GamerSelectedMoveEvent;
import org.ggp.base.player.gamer.exception.GamePreviewException;
import org.ggp.base.player.gamer.statemachine.GameAnalyser;
import org.ggp.base.player.gamer.statemachine.StateMachineGamer;
import org.ggp.base.player.gamer.statemachine.mcts.tree.MonteCarloTree;
import org.ggp.base.util.game.Game;
import org.ggp.base.util.logging.GamerLogger;
import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.StateMachine;
import org.ggp.base.util.statemachine.cache.CachedStateMachine;
import org.ggp.base.util.statemachine.exceptions.GoalDefinitionException;
import org.ggp.base.util.statemachine.exceptions.MoveDefinitionException;
import org.ggp.base.util.statemachine.exceptions.TransitionDefinitionException;
import org.ggp.base.util.statemachine.implementation.prover.ProverStateMachine;

import java.util.List;

/**
 * User: Peara
 * Date: 1/22/15
 * Time: 9:31 PM
 */
public class MCTSGamer extends StateMachineGamer {
    MonteCarloTree searcher;
    GameAnalyser analyser;

    @Override
    public StateMachine getInitialStateMachine() {
        // TODO this will return a PropnetStateMachine later
        return new CachedStateMachine(new ProverStateMachine());
        // return new CachedStateMachine(new PropNetStateMachine());
    }

    @Override
    public void stateMachineMetaGame(long timeout) throws TransitionDefinitionException, MoveDefinitionException, GoalDefinitionException {
        searcher = new MonteCarloTree(0.87, getStateMachine(), getMatch());
        analyser = new GameAnalyser(getStateMachine());

        GamerLogger.log("MCTSGamer", "this is a two-player game: " + String.valueOf(analyser.is2PGame()));
        GamerLogger.log("MCTSGamer", "this is a two-player turn-based game: " + String.valueOf(analyser.isTurnBase2PGame()));
    }

    @Override
    public Move stateMachineSelectMove(long timeout) throws TransitionDefinitionException, MoveDefinitionException, GoalDefinitionException {
        long start = System.currentTimeMillis();
        long finishBy = timeout - 1000;

        Move selection = searcher.selectBestMove(getCurrentState(), getRole(), finishBy);

        long stop = System.currentTimeMillis();

        List<Move> moves = getStateMachine().getLegalMoves(getCurrentState(), getRole());
        notifyObservers(new GamerSelectedMoveEvent(moves, selection, stop - start));
        return selection;
    }

    @Override
    public void stateMachineStop() {
        //TODO nothing for now
    }

    @Override
    public void stateMachineAbort() {
        //TODO nothing for now
    }

    @Override
    public void preview(Game g, long timeout) throws GamePreviewException {
        //TODO more later
    }

    @Override
    public String getName() {
        return "Kaao-akai MCTS";
    }
}
