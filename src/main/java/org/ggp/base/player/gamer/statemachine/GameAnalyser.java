package org.ggp.base.player.gamer.statemachine;

import org.ggp.base.player.gamer.exception.GamePreviewException;
import org.ggp.base.util.game.Game;
import org.ggp.base.util.logging.GamerLogger;
import org.ggp.base.util.statemachine.MachineState;
import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.statemachine.StateMachine;
import org.ggp.base.util.statemachine.cache.CachedStateMachine;
import org.ggp.base.util.statemachine.exceptions.GoalDefinitionException;
import org.ggp.base.util.statemachine.exceptions.MoveDefinitionException;
import org.ggp.base.util.statemachine.exceptions.TransitionDefinitionException;
import org.ggp.base.util.statemachine.implementation.prover.ProverStateMachine;

import java.util.ArrayList;
import java.util.List;

/**
 * Peara - 1/30/16.
 */
public class GameAnalyser {
    StateMachine m_state_machine;
    Role m_first_player, m_second_player;

    public GameAnalyser(StateMachine state_machine) {
        m_state_machine = state_machine;
    }


    public Role getFirstPlayer() {
        return m_first_player;
    }

    public Role getSecondPlayer() {
        return m_second_player;
    }

    /**
     * Check if the game has 2 players
     * @return
     */
    public boolean is2PGame() {
        return m_state_machine.getRoles().size() == 2;
    }

    /**
     * Check if the game is a turn-based 2-player game
     * @return
     */
    public boolean isTurnBase2PGame() {
        if (is2PGame()) {
            // randomly play the game and get move lists of each states
            // if for any state, a player has only one move while the other has more than one -> turn based
            // also identify the first player's role
            // enough for now

            MachineState state = m_state_machine.getInitialState();
            List<Integer> move_count_0 = new ArrayList<>(); // count the number of moves for role 0
            List<Integer> move_count_1 = new ArrayList<>(); // count the number of moves for role 1
            Role r0 = m_state_machine.getRoles().get(0);
            Role r1 = m_state_machine.getRoles().get(1);

            try {
                while (!m_state_machine.isTerminal(state)) {
                    move_count_0.add(m_state_machine.getLegalMoves(state, r0).size());

                    move_count_1.add(m_state_machine.getLegalMoves(state, r1).size());
                    state = m_state_machine.getNextStateDestructively(state, m_state_machine.getRandomJointMove(state));
                }
            } catch (Exception e) {
                GamerLogger.logError("GameAnalyser", "EXCEPTION: " + e.getMessage());
            }

            if (move_count_1.get(0) == 1) {
                // guess
                m_first_player = r0;
                m_second_player = r1;
                // check
                boolean approved = true;
                for (int i=0; i<move_count_0.size(); i++) {
                    // even: first player turn - r0
                    // odd: second player turn - r1
                    if (i%2 == 0) {
                        if (move_count_1.get(i) != 1) { // -- r1 is second
                            approved = false;
                            break;
                        }
                    } else {
                        if (move_count_0.get(i) != 1) {
                            approved = false;
                            break;
                        }
                    }
                }
                if (approved) {
                    return true;
                }

            }

            if (move_count_0.get(0) == 1) {
                m_first_player = r1;
                m_second_player = r0;

                boolean approved = true;
                for (int i=0; i<move_count_0.size(); i++) {
                    // even: first player turn - r1
                    // odd: second player turn - r0
                    if (i%2 == 0) {
                        if (move_count_0.get(i) != 1) { // -- r0 is second
                            approved = false;
                            break;
                        }
                    } else {
                        if (move_count_1.get(i) != 1) {
                            approved = false;
                            break;
                        }
                    }
                }
                if (approved) {
                    return true;
                }
            }
        }
        return false;
    }
}
