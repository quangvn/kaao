package org.ggp.base.player.gamer.statemachine.mcts.tree;

import org.ggp.base.player.gamer.statemachine.cache.TranspositionTable;
import org.ggp.base.util.logging.GamerLogger;
import org.ggp.base.util.match.Match;
import org.ggp.base.util.statemachine.MachineState;
import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.statemachine.StateMachine;
import org.ggp.base.util.statemachine.exceptions.GoalDefinitionException;
import org.ggp.base.util.statemachine.exceptions.MoveDefinitionException;
import org.ggp.base.util.statemachine.exceptions.TransitionDefinitionException;

import java.util.*;

/**
 * Peara - 2/2/16.
 *
 *
 */
public class MinimaxTree {
    long timeout;
    StateMachine theMachine;
    MachineState theState;
    Match theMatch;
    List<Role> roles;
    int m_turn; // index of the active role (0 or 1)
    Role m_first_player, m_second_player;
    MXNode root;
    int depth;
//    TranspositionTable<MXNode> cache;

    public void setFirstPlayer(Role r) {
        m_first_player = r;
    }

    public void setSecondPlayer(Role r) {
        m_second_player = r;
    }

    public void setTurn(int turn) {
        m_turn = turn;
    }

    /**
     * Tree-structure class
     */
    private class MXNode {
        MachineState state;
        int visitCount;
        int totalScore;
        Role m_active_role; // the role has control of current state
        int m_active_role_id; // id of the active role
        Role m_passive_role;
        HashMap<Move, MXNode> stateInfo = null;
        public List<Move> m_move_list = null;

        public MXNode(MachineState _state, Role active_role) {
            state = _state;
            visitCount = 0;
            totalScore = 0;
            m_active_role = active_role;
            if (m_active_role.equals(theMachine.getRoles().get(0))) {
                m_active_role_id = 0;
                m_passive_role = theMachine.getRoles().get(1);
            } else {
                m_active_role_id = 1;
                m_passive_role = theMachine.getRoles().get(0);
            }
        }

        public double getWinRate() {
            if (visitCount > 0) {
                return (double) totalScore / visitCount;
            }
            return 0;
        }

        /**
         * Expand this node
         * @throws MoveDefinitionException
         */
        private void expand()
                throws MoveDefinitionException {
            stateInfo = new HashMap<Move, MXNode>();
            // if this is a terminal state, no need for any moves
            if (theMachine.isTerminal(this.state)) {
                return;
            }
            m_move_list = theMachine.getLegalMoves(state, m_active_role);
            Move noop = theMachine.getLegalMoves(state, m_passive_role).get(0);

            List<Move> moves = new ArrayList<>();
            for (Move move : m_move_list) {
                // generate move combination
                moves.clear();
                if (m_active_role_id == 0) {
                    moves.add(move);
                    moves.add(noop);
                } else {
                    moves.add(noop);
                    moves.add(move);
                }
                try {
                    stateInfo.put(move, new MXNode(theMachine.getNextState(state, moves), m_passive_role));
                } catch (TransitionDefinitionException e) {
                    GamerLogger.logError("MCTree2PG", "Expansion error: " + e.getMessage());
                }
            }
        }


        /**
         * Best move selection
         * @param role
         * @return
         */
        public Move selectBestMove(Role role) throws MoveDefinitionException {
            if (role.equals(m_passive_role)) {
                return theMachine.getLegalMoves(state, role).get(0);
            }

            double min_op_winrate = 100;
            Move res = null;
            for (Map.Entry<Move, MXNode> entry : stateInfo.entrySet()) {
                if (entry.getValue().getWinRate() < min_op_winrate) {
                    min_op_winrate = entry.getValue().getWinRate();
                    res = entry.getKey();
                }
            }
            return res;
        }
    }

    public MinimaxTree(double _expCoeff, StateMachine _machine, Match _match)
            throws MoveDefinitionException {
        theMachine = _machine;
        theMatch = _match;
        roles = theMachine.getRoles();
        // cache = new TranspositionTable<MXNode>();
    }

    public Move selectBestMove(MachineState state, Role role, long timeout, Role active_role)
            throws MoveDefinitionException, TransitionDefinitionException, GoalDefinitionException {

        // update to the current game state
        theState = state;
        /*if (cache.contain(theState, lastMoves)) {
            root = cache.get(theState, lastMoves);
        } else {
*/
        root = new MXNode(theState, active_role);
//            cache.put(theState, lastMoves, root);
        root.expand();
//        GamerLogger.log("MCTree2PG", "number of moves: " + root.m_move_list.size());
//        GamerLogger.log("MCTree2PG", "current role: " + role.toString());
//        GamerLogger.log("MCTree2PG", "active role: " + active_role.toString());
//        }


        Move selected_move = root.selectBestMove(role);

        GamerLogger.log("MCTree2PG", "selected move: " + selected_move.toString());
        return selected_move;
    }
}
